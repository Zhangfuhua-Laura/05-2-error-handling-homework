package com.twuc.webApp.web;

import com.twuc.webApp.domain.mazeRender.RenderType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class MazeIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_maze_when_send_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/mazes/grass"));
    }

    @Test
    void should_get_400_and_exception_massage_when_there_is_an_unhandled_exception() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/mazes/grass?width=0"))
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.jsonPath("massage").value("The width(0) is not valid"));
    }
}
