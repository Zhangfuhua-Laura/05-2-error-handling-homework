package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class MazeController {
    private final GameLevelService gameLevelService;

    public MazeController(GameLevelService gameLevelService) {
        this.gameLevelService = gameLevelService;
    }

    private static final Logger logger = LoggerFactory.getLogger(MazeController.class);

    @GetMapping("/mazes/{type}")
    @ResponseStatus(HttpStatus.OK)
    public void getMaze2(
        HttpServletResponse response,
        @RequestParam(required = false, defaultValue = "10") int width,
        @RequestParam(required = false, defaultValue = "10") int height,
        @PathVariable String type) throws IOException {

        response.setContentType(MediaType.IMAGE_PNG_VALUE);

        gameLevelService.renderMaze(
            response.getOutputStream(),
            width,
            height,
            type
        );
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException illegalArgumentException){
        logger.error("It is IllegalArgumentException error!!!");
        return ResponseEntity.status(400).contentType(MediaType.APPLICATION_JSON).body(String.format("{\"massage\":\"%s\"}", illegalArgumentException.getMessage()));
    }
}
